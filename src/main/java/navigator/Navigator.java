package navigator;

import main.PersonalMessage;
import org.jsoup.Connection;

import java.io.*;
import java.util.List;
import java.util.Properties;

public abstract class Navigator {
    String cssMessageChain = "table.tablebg > tbody > tr > td > div > table > tbody > tr";
    String cssSingleMessage = "#pagecontent table.tablebg tr.row1 td.gen";
    String cssUrl = "div.gensmall > a";
    String cssPostbody = "div.postbody";
    String cssNextPage = "#pagecontent > table > tbody > tr > td > table > tbody > tr > td a";

    String site;
    String userAgent;
    String loginFormUrl;
    String currentPageAddress;

    // property names from file with settings
    final String pSite = "site";
    final String pUsername = "username";
    final String pPassword = "password";
    final String pUserAgent = "user_agent";
    final String pLoginFormUrl = "login_form";
    final String pLoginActionUrl = "login_action";
    final String pLoginStyle = "login_by";

    protected abstract void loginWithCookies(Properties p) throws IOException;

    protected abstract void loginWithPassword (Properties p);

    public abstract List<String> getUrls(String pageAddress, String selector);

    public abstract List<String> getTexts(String pageAddress, String selector);

    public abstract String getNextPageLink(String address);

    public abstract List<PersonalMessage> getMessageChain(String address);

    public abstract String getPHPBBCode(String address);

    public abstract void stop ();

    protected void savePageToFile(String page, String filename) {
        File f = new File (filename + ".html");
        try {
            Writer out = new BufferedWriter(new FileWriter(f));
            out.append(page);
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
