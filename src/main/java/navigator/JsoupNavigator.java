package navigator;

import main.PersonalMessage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.*;

public class JsoupNavigator extends Navigator {

    private HashMap<String, String> cookies;
    private Connection.Response currentPage;


    public JsoupNavigator(Properties p) throws IOException {
        site = p.getProperty(pSite);
        userAgent = p.getProperty(pUserAgent);
        cookies = new HashMap<>();
        if ("cookie".equals(p.getProperty(pLoginStyle))) loginWithCookies(p);
                                                    else loginWithPassword(p);
    }

    @Override
    protected void loginWithPassword(Properties p) {
        loginFormUrl = p.getProperty(pLoginFormUrl);
        String loginActionUrl = p.getProperty(pLoginActionUrl);
        String username = p.getProperty(pUsername);
        String password = p.getProperty(pPassword);
        String sid;

        HashMap<String, String> formData = new HashMap<>();
        Connection.Response loginForm;
        try {
            loginForm = Jsoup.connect(loginFormUrl).method(Connection.Method.GET).userAgent(userAgent)
                    .header("Accept", "text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Encoding", "gzip, deflate, br")
                    .header("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
                    .header("Connection", "keep-alive")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("DNT", "1")
                    .header("Host", "dxdy.ru")
                    .header("Referer", "https://dxdy.ru/ucp.php?mode=login")
                    .header("Upgrade-Insecure-Requests", "1")
                    .execute();
            Document doc = loginForm.parse();
            savePageToFile(doc.outerHtml(), "loginform");


            cookies.putAll(loginForm.cookies());
            saveCookies(loginForm.cookies(), "cookies1");

            sid = doc.select("input[name=sid]").first().val();
            System.out.println("sid = " + sid);
            formData.put("autologin", "on");
            formData.put("login", "Вход");
            formData.put("password", password);
            formData.put("redirect", "index.php");
            formData.put("sid", sid);
            formData.put("username", username);

            Connection.Response homePage = Jsoup.connect(loginActionUrl)
                    .cookies(cookies).data(formData).method(Connection.Method.POST)
                    .userAgent(userAgent)
                    .header("Accept", "text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8")
                    .header("Accept-Encoding", "gzip, deflate, br")
                    .header("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
                    .header("Connection", "keep-alive")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("DNT", "1")
                    .header("Host", "dxdy.ru")
                    .header("Referer", "https://dxdy.ru/ucp.php?mode=login")
                    .header("Upgrade-Insecure-Requests", "1")
                    .execute();
            cookies.putAll(homePage.cookies());
            saveCookies(homePage.cookies(), "cookies2");

            savePageToFile(homePage.parse().outerHtml(), "homepage");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getUrls(String pageAddress, String selector) {
        List<String> l = new ArrayList<>();
        if (pageAddress != null) {
            try {
                Elements els = getPage(pageAddress).parse().select(selector);
                for (Element e : els) l.add(e.attr("href"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return l;
    }

    @Override
    public List<String> getTexts(String pageAddress, String selector) {
        List<String> l = new ArrayList<>();
        if (pageAddress != null) {
            try {
                Elements els = getPage(pageAddress).parse().select(selector);
                for (Element e : els) l.add(e.text());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return l;
    }

    @Override
    protected void loginWithCookies(Properties p) throws IOException {
        Properties c = new Properties();
        FileInputStream f = new FileInputStream("cookies.txt");
        c.load(f);
        f.close();
        cookies.put("phpbb3_odcb2_sid", c.getProperty("phpbb3_odcb2_sid"));
        cookies.put("phpbb3_odcb2_k", c.getProperty("phpbb3_odcb2_k"));
        cookies.put("phpbb3_odcb2_u", c.getProperty("phpbb3_odcb2_u"));
    }

    @Override
    public String getNextPageLink(String address) {
        String nextPageLink = "";
        try {
            Element e = getPage(address).parse().select(cssNextPage).last();
            if (e != null) nextPageLink = (e.text().equals("След.")) ? e.attr("href") : "";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return nextPageLink;
    }

    @Override
    public List<PersonalMessage> getMessageChain(String address) {

        List<PersonalMessage> list = new ArrayList<>();

        try {
            Elements el = getPage(address).parse().select(cssMessageChain);
            PersonalMessage p;

            if (el.size() == 0) {
                String url = getPage(address).parse().select(cssUrl).get(1).attr("href");
                Elements singleMessage = getPage(address).parse().select(cssSingleMessage);

                p = new PersonalMessage();
                p.id = url.substring(url.indexOf("&p=") + 3);
                p.topic = singleMessage.get(0).text();
                p.author = singleMessage.get(1).text();
                p.date = singleMessage.get(2).text();
                p.htmlBody = getPage(address).parse().select(cssPostbody).outerHtml();
                list.add(p);
            }
            for (int i = 1; i < el.size(); i += 4) {
                p = new PersonalMessage();
                p.id = el.get(i).select("td > a").last().attr("name");
                p.topic = el.get(i).select("td > div.gensmall").first().text();
                p.author = el.get(i).select("span.postauthor a").last().text();
                p.date = el.get(i + 1).select("td > table > tbody > tr > td > table > tbody > tr[valign=middle] > td.gensmall").last().text();
                p.htmlBody = el.get(i + 1).select("div.postbody").outerHtml();
                list.add(p);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public String getPHPBBCode(String address) {
        try {
            return getPage(address).parse().getElementById("msg").val();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void stop() {

    }

    Connection.Response getPage(String address) {
        if (address.equals(currentPageAddress)) return currentPage;

        int attempt = 0;
        currentPage = null;

        String actualAddress = ("./".equals(address.substring(0, 2))) ? site + address.substring(1) : address;

        while (attempt++ < 3 && currentPage == null) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            try {
                currentPage = Jsoup.connect(actualAddress).cookies(cookies).method(Connection.Method.GET).userAgent(userAgent).execute();
//                Document d = Jsoup.connect(actualAddress).cookies(cookies).userAgent(userAgent).get();
//                savePageToFile(d.outerHtml(), "tmp.html");
            } catch (IOException e) {
                System.out.println("Can't reach the page: " + address + ", attempt " + attempt + ", trying again");
            }
        }

        currentPageAddress = address;
        return currentPage;
    }

    private void saveCookies (Map<String, String> cookies, String filename) {
        File f = new File (filename + ".txt");

        try {
            Writer out = new BufferedWriter(new FileWriter(f));
            for (Map.Entry e : cookies.entrySet()) {
                out.append(e.getKey() + ":" + e.getValue() + "\n");
            }
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}