package navigator;

import main.PersonalMessage;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SeleniumNavigator extends Navigator {
    WebDriver driver;
    private String currentPage;

    public SeleniumNavigator(Properties p) {
        String pDriverType = "driver_type";
        String pDriverPath = "path";
        System.setProperty(p.getProperty(pDriverType), p.getProperty(pDriverPath));
        driver = new ChromeDriver();
        loginWithPassword(p);
    }

    @Override
    protected void loginWithPassword(Properties p) {
        goToPage(p.getProperty(pLoginFormUrl));

        driver.findElement(By.cssSelector("input[name=username]")).sendKeys(p.getProperty(pUsername));
        driver.findElement(By.cssSelector("input[name=password]")).sendKeys(p.getProperty(pPassword));
        driver.findElement(By.cssSelector("input.btnmain")).click();
    }

    @Override
    public List<String> getUrls(String pageAddress, String selector) {
        List<String> l = new ArrayList<>();
        if (pageAddress != null) {
            goToPage(pageAddress);
            List<WebElement> els = driver.findElements(By.cssSelector(selector));
            if (els != null) for (WebElement e : els) l.add(e.getAttribute("href"));
        }
        return l;
    }

    @Override
    public List<String> getTexts(String pageAddress, String selector) {
        List<String> l = new ArrayList<>();
        goToPage(pageAddress);
        List<WebElement> els = driver.findElements(By.cssSelector(selector));
        if (els != null) for (WebElement e : els) l.add(e.getText());
        return l;
    }

    @Override
    protected void loginWithCookies(Properties p) throws IOException {

    }

    @Override
    public String getNextPageLink(String address) {
        String nextPageLink = "";
        goToPage(address);
        List<WebElement> els = driver.findElements(By.cssSelector(cssNextPage));
        if (els != null) {
            WebElement e = els.get(els.size() - 1);
            nextPageLink = (e.getText().equals("След.")) ? e.getAttribute("href") : "";
        }
        return nextPageLink;

    }

    @Override
    public List<PersonalMessage> getMessageChain(String address) {
        List<PersonalMessage> list = new ArrayList<>();

        goToPage(address);

            List<WebElement> el = driver.findElements(By.cssSelector(cssMessageChain));
            PersonalMessage p;

            if (el.size() == 0) {
                String url = driver.findElements(By.cssSelector(cssUrl)).get(1).getAttribute("href");
                List<WebElement> singleMessage = driver.findElements(By.cssSelector(cssSingleMessage));

                p = new PersonalMessage();
                p.id = url.substring(url.indexOf("&p=") + 3);
                p.topic = singleMessage.get(0).getText();
                p.author = singleMessage.get(1).getText();
                p.date = singleMessage.get(2).getText();
                p.htmlBody = driver.findElement(By.cssSelector(cssPostbody)).getAttribute("outerHTML");
                list.add(p);
            }
            for (int i = 1; i < el.size(); i += 4) {
                p = new PersonalMessage();
                p.id =       getLast(el.get(i), "td > a").getAttribute("name");
                p.topic =    getFirst(el.get(i), "td > div.gensmall").getText();
                p.author =   getLast(el.get(i), "span.postauthor a").getText();
                p.date =     getLast(el.get(i + 1), "td > table > tbody > tr > td > table > tbody > tr[valign=middle] > td.gensmall").getText();
                p.htmlBody = getFirst(el.get(i + 1), "div.postbody").getAttribute("outerHTML");
                list.add(p);
            }

        return list;
    }

    private WebElement getFirst(WebElement root, String css) {
        List<WebElement> l = root.findElements(By.cssSelector(css));
        return l.get(0);
    }

    private WebElement getLast(WebElement root, String css) {
        List<WebElement> l = root.findElements(By.cssSelector(css));
        return l.get(l.size() - 1);
    }

    @Override
    public String getPHPBBCode(String address) {
        goToPage(address);
        return driver.findElement(By.id("msg")).getAttribute("value");
    }

    @Override
    public void stop() {
        driver.quit();
    }

    private void goToPage (String address) {
        if (address == null) return;
        if (address.equals(currentPage)) return;

        int attempt = 0;
        currentPage = null;

        String actualAddress = ("./".equals(address.substring(0, 2))) ? site + address.substring(1) : address;

        while (attempt++ < 3 && currentPage == null) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

            driver.get(address);
        }
        currentPage = address;
    }
}
