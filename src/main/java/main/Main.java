package main;

import navigator.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Main {
    private static Navigator navigator;
    private static List<MessageChain> chains;

    public static void main (String[] args) {
        try {
            System.out.println("Initialization");
            Properties p = loadSettings("settings.ini");

            switch (p.getProperty("method")) {
                case "jsoup" : {
                    navigator = new JsoupNavigator(p);
                    break;
                }
                case "selenium" : {
                    navigator = new SeleniumNavigator(p);
                    break;
                }
            }

            scanPM();
            navigator.stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void scanPM () {
        String cssFolders = "#wrapcentre ul.nav li a";
        List<String> folders;
        List<String> folderNames;
        String pmPage = "https://dxdy.ru/ucp.php?i=pm&folder=inbox";

        folders = navigator.getUrls(pmPage, cssFolders);
        folderNames = navigator.getTexts(pmPage, cssFolders);

        System.out.println("PM folders found: " + (folders.size() - 4));

        for (int i = 0; i < folders.size(); i++) scanFolder(folders.get(i), folderNames.get(i));

    }

//    private static void saveFolder(String name) {
//        Saver saver = new Saver();
//        for (MessageChain m : chains) {
//            saver.saveChain(m);
//        }
//    }
//
    private static void scanFolder (String folderAddress, String folderName) {
        chains = new ArrayList<>();
        System.out.println("Scanning folder " + folderAddress);

        // check is it correct folder
        int fBegin = folderAddress.indexOf("&folder=");
        String f = (fBegin >= 0) ? folderAddress.substring(fBegin + 8) : "outbox";
        if ("outbox".equals(f) || fBegin == -1) return;

        String cssMessageLink = "#pagecontent > form:nth-child(3) > table > tbody > tr > td > span.topictitle a";
        List<String> messageLinks;
        String nextPage = folderAddress;

        while (!"".equals(nextPage)) {
            System.out.println("    Scanning page " + nextPage);

            messageLinks = navigator.getUrls(nextPage, cssMessageLink);

            for (String link: messageLinks) {
                scanMessageChain(link, folderName);
            }
//
            nextPage = navigator.getNextPageLink(nextPage);
        }
    }

    private static void scanMessageChain(String link, String folderName) {
        String messageID = link.substring(link.indexOf("&p=") + 3);
        if (isInChain(messageID)) return;

        System.out.println("        Chain " + link);

        MessageChain newChain = new MessageChain(folderName);
        newChain.addMessages(navigator.getMessageChain(link));

        String phpbbURL = "https://dxdy.ru/ucp.php?i=pm&mode=compose&action=quote&f=0&p=";
        for (PersonalMessage pm: newChain.messages) {
            pm.phpbbBody = navigator.getPHPBBCode(phpbbURL + pm.id);
        }
        new Saver().saveChain(newChain);
    }

    private static boolean isInChain (String messageID) {
        boolean result = false;

        for (MessageChain m: chains) {
            result = result || (m.indexOf(messageID) != -1);
        }
        return result;
    }

    private static Properties loadSettings(String filename) throws IOException {
        Properties p = new Properties();
        FileInputStream f = new FileInputStream(filename);
        p.load(f);
        f.close();
        return p;
    }
}