package main;

import java.io.*;

public class Saver {
    private String delimiter;
    private String htmlStart = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" xml:lang=\"ru-ru\" class=\"gr__dxdy_ru\" lang=\"ru-ru\"><head>\n" +
            "<meta charset=\"utf-8\">" +
            "<link href=\"123_files/font-awesome.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\">\n" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://dxdy-css.korotkov.co.uk/styles/subsilver2/theme/stylesheet-mini-005.css\"></head>\n" +
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"dop.css\"></head>\n" +
            "<body class=\"ltr\" data-gr-c-s-loaded=\"true\">";
    private String htmlEnd = "</body></html>";
    private String messageTemplate = "<div class=\"message-container\">\n" +
            "  <div id=\"id-#ID#\"><span>#AUTHOR#</span><span>#DATE#</span><span>#TOPIC#</span>\n" +
            "    #POST#\n" +
            "    <textarea id=code-#ID#>#BBCODE#</textarea>\n" +
            "  </div>\n" +
            "</div>";

    void saveChain(MessageChain chain) {
        if (!prepareFolder(chain.folder)) {
            System.out.println("Chain ID = " + chain.startMessage + " not saved. Can't use folder " + chain.folder);
            return;
        }
        File f = new File (new File (chain.folder), chain.startMessage + " - " + chain.messages.get(chain.messages.size() - 1).author + ".html");

        try {
            Writer out = new BufferedWriter(new FileWriter(f));
            out.append(htmlStart);
            for (int i = chain.messages.size() - 1; i >= 0; i--) {
                PersonalMessage p = chain.messages.get(i);
                out.append(messageTemplate
                        .replace("#ID#", p.id)
                        .replace("#AUTHOR#", p.author)
                        .replace("#DATE#", p.date)
                        .replace("#TOPIC#", p.topic)
                        .replace("#POST#", p.htmlBody)
                        .replace("#BBCODE#", p.phpbbBody)
                );
            }
            out.append(htmlEnd);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean prepareFolder (String name) {
        File file = new File(name);
        boolean result;

        result = file.exists();
        if (!result) result = file.mkdir();
        return result;
    }
}
