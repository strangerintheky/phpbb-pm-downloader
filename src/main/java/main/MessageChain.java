package main;

import java.util.ArrayList;
import java.util.List;

class MessageChain {
    public List<PersonalMessage> messages;
    public String startMessage;
    public String folder;

    MessageChain (String f) {
        messages = new ArrayList<>();
        folder = f;
    }

    void addMessages (List<PersonalMessage> list) {
        messages.addAll(list);
        startMessage = messages.get(messages.size() - 1).id;

//        for (main.PersonalMessage p: list) {
//            System.out.println("            Message id " + p.id + " author " + p.author + " date " + p.date + " topic " + p.topic);
//        }
    }

    int indexOf(String messageID) {
        int i = -1;
        if (messages.size() > 0) while (++i < messages.size() && !messages.get(i).id.equals(messageID));
        return (i == messages.size()) ? -1 : i;
    }
}
